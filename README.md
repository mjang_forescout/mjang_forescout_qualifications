# Mike Jang's Qualifications

Mike Jang's qualifications for the position.

- Resume: mjangResume_Forescout.docx
- Writing Samples: writingSamples.md
- Cover Letter: coverLetter.docx
- Speaking Experience: speakingExperience.md

